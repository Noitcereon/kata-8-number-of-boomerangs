console.log("Starting");
let logIdentifier = 1;



function countBoomerangs(numberArray) {
    if (Array.isArray(numberArray) === false) {
        console.warn("Invalid input");
        return 0;
    }
    let boomerangCounter = 0;
    let arrayToCheck = [];
    for (let indexOfNumber in numberArray) {
        indexOfNumber = Number(indexOfNumber);
        if (indexOfNumber + 1 == numberArray.length) {
            // reset array
            arrayToCheck = [];
            break;
        }
        // add number to array used for boomerang checks
        arrayToCheck.push(numberArray[indexOfNumber]);

        // if 3 numbers have been iterated over
        if (indexOfNumber > 2) {
            // remove the first element of the check array.
            arrayToCheck.shift();
        }
        // check for boomerang
        const isBoomerang = checkForBoomerang(arrayToCheck);

        if (isBoomerang) {
            // Add a boomerang to counter.
            boomerangCounter++;
        }
    }
    return boomerangCounter;
}
function checkForBoomerang(arrayToCheck) {
    return arrayToCheck[0] === arrayToCheck[2] & arrayToCheck[1] !== arrayToCheck[0];
}


verbalBoomerangLog(countBoomerangs([9, 5, 9, 5, 1, 1, 1])) // ➞ 2
verbalBoomerangLog(countBoomerangs([5, 6, 6, 7, 6, 3, 9])) // ➞ 1
verbalBoomerangLog(countBoomerangs([4, 4, 4, 9, 9, 9, 9]))// ➞ 0
verbalBoomerangLog(countBoomerangs([1, 7, 1, 7, 1, 7, 1])) //➞ 5

console.log("Stopping");

function verbalBoomerangLog(callback) {
    console.log(`Case ${logIdentifier}`)
    console.log("No. of boomerangs:", callback);
    logIdentifier++;
}